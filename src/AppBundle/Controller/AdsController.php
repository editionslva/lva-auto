<?php
// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdsController extends Controller
{
    /**
     * @Route("/petites-annonces")
     */
    public function indexAction()
    {
        $number = mt_rand(0, 100);

        return $this->render('ads/index.html.twig', array(
            'number' => $number,
        ));
    }
	
	/**
     * @Route("/petites-annonces/{page}")
     */
    public function listAction($page)
    {
         return $this->render('ads/list.html.twig', array(
            'page' => $page,
        ));
    }
	
	/**
     * @Route("/petites-annonces/{slug}")
     */
    public function viewAction($slug)
    {
         throw new \Exception('Something went wrong!');
		 return $this->render('ads/view.html.twig', array(
            'slug' => $slug,
        ));
    }
}